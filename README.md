# README #

A project based on project 1 that uses Flask and Docker.

The software sets up a server that can serve pages. It is able to recognize forbidden pages and pages that are not found.

## Author: Dylan Conway, dconway@uoregon.edu ##
