from flask import Flask
from flask import render_template, abort
import os

app = Flask(__name__)

@app.route("/<path:path>")
def path_server(path):
    if("//" in path):
        abort(403)
    elif("/~" in path):
        abort(403)
    elif("/.." in path):
        abort(403)
    elif(os.path.isfile("templates/" + path)):
        return render_template(path)
    else:
        return render_template("404.html"), 404

@app.errorhandler(403)
def forbidden(error):
    return render_template("403.html"), 403

def main():
    app.run(debug=True, host='0.0.0.0')

if __name__ == "__main__":
    main()

